package uas.project.submissionmade2.Activity;

import android.content.ContentValues;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import uas.project.submissionmade2.Entity.Movies;
import uas.project.submissionmade2.Entity.TvSeries;
import uas.project.submissionmade2.Fragment.LoadFavoriteCallback;
import uas.project.submissionmade2.R;

import static android.provider.BaseColumns._ID;
import static uas.project.submissionmade2.Database.DatabaseContract.FavColumn.COL_2;
import static uas.project.submissionmade2.Database.DatabaseContract.FavColumn.COL_3;
import static uas.project.submissionmade2.Database.DatabaseContract.FavColumn.COL_4;
import static uas.project.submissionmade2.Database.DatabaseContract.FavColumn.COL_5;
import static uas.project.submissionmade2.Database.DatabaseContract.FavColumn.COL_6;
import static uas.project.submissionmade2.Database.DatabaseContract.FavColumn.COL_7;
import static uas.project.submissionmade2.Database.DatabaseContract.FavColumn.COL_8;
import static uas.project.submissionmade2.Database.DatabaseContract.FavColumn.COL_9;
import static uas.project.submissionmade2.Database.DatabaseContract.FavColumn.CONTENT_URI;

public class DetailActivity extends AppCompatActivity implements View.OnClickListener, LoadFavoriteCallback {

    private TextView tvName,tvDate,tvOverview,tvPopular,tvRating;
    private ImageView ivPoster,ivBackdrop;
    private RatingBar ratingBar;
    private ProgressBar progressBar;
    private boolean isFavorited;
    private ImageButton btnFav;
    private int tipe;
    private static int idDetail;

    public static final String DETAIL_DATA_MOVIES= "detail_data_movies";
    public static final String DETAIL_DATA_TVSERIES= "detail_data_tvseries";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        HandlerThread handlerThread;
        DataObserverr myObserver;

        tvName = findViewById(R.id.tvName);
        tvDate = findViewById(R.id.tvDate);
        tvOverview = findViewById(R.id.tvOverview);
        tvPopular = findViewById(R.id.tvPopular);
        tvRating = findViewById(R.id.tvRating);

        ivPoster = findViewById(R.id.ivPoster);
        ivBackdrop = findViewById(R.id.ivBackdrop);

        ratingBar = findViewById(R.id.ratingBar);
        progressBar = findViewById(R.id.progressDetail);
        btnFav = findViewById(R.id.btnFav);
        btnFav.setOnClickListener(this);

        tipe = getIntent().getIntExtra("type",0);

        handlerThread = new HandlerThread("DataObserverr");
        handlerThread.start();

        Handler handler = new Handler(handlerThread.getLooper());
        myObserver = new DataObserverr(handler, this);
        this.getContentResolver().registerContentObserver(CONTENT_URI, true, myObserver);

        if(getIntent().getParcelableExtra(DETAIL_DATA_MOVIES) != null){
            showMovies();
        }
        else showTvSeries();

    }

    private void showTvSeries() {
        final TvSeries tvSeries = getIntent().getParcelableExtra(DETAIL_DATA_TVSERIES);
        idDetail = tvSeries.getTvId();

        checkFavorite(idDetail);
        Glide.with(this)
                .load( "https://image.tmdb.org/t/p/w600_and_h900_bestv2" + tvSeries.getTvBackDrop())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        tvName.setText(tvSeries.getTvName());
                        tvDate.setText(tvSeries.getTvDate());
                        tvOverview.setText(tvSeries.getTvOverview());
                        tvPopular.setText("  "+String.valueOf(tvSeries.getTvPopularity()));
                        tvRating.setText(String.valueOf(tvSeries.getTvRate()));
                        ratingBar.setRating((float) tvSeries.getTvRate());

                        Glide.with(getApplicationContext())
                                .load( "https://image.tmdb.org/t/p/w600_and_h900_bestv2" + tvSeries.getTvPoster())
                                .into(ivPoster);

                        if(isFavorited){
                            Drawable new_image= getResources().getDrawable(R.drawable.ic_favorite_pink_28dp);
                            btnFav.setImageDrawable(new_image);
                        }

                        progressBar.setVisibility(View.GONE);

                        return false;
                    }
                })
                .into(ivBackdrop);
    }

    private void showMovies() {
        final Movies movie = getIntent().getParcelableExtra(DETAIL_DATA_MOVIES);
        idDetail = movie.getMovieId();

        checkFavorite(idDetail);
        Glide.with(this)
                .load( "https://image.tmdb.org/t/p/w600_and_h900_bestv2" + movie.getMovieBackDrop())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        tvName.setText(movie.getMovieName());
                        tvPopular.setText(String.valueOf(movie.getMoviePopularity()));
                        tvOverview.setText(movie.getMovieOverview());
                        tvDate.setText(movie.getMovieDate());
                        tvRating.setText(String.valueOf( movie.getMovieRate()));
                        ratingBar.setRating((float) movie.getMovieRate());

                        Glide.with(getApplicationContext())
                                .load( "https://image.tmdb.org/t/p/w600_and_h900_bestv2" + movie.getMoviePoster())
                                .into(ivPoster);

                        if(isFavorited){
                            Drawable new_image= getResources().getDrawable(R.drawable.ic_favorite_pink_28dp);
                            btnFav.setImageDrawable(new_image);
                        }

                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(ivBackdrop);

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnFav) {

            Drawable isFav = getResources().getDrawable(R.drawable.ic_favorite_pink_28dp);
            Drawable notFav = getResources().getDrawable(R.drawable.ic_favorite_border_pink_28dp);

            if (isFavorited) {
                if (tipe == 0) {
                    final Movies movie = getIntent().getParcelableExtra(DETAIL_DATA_MOVIES);
                    getContentResolver().delete(Uri.parse(CONTENT_URI + "/ID/" + String.valueOf(movie.getMovieId())), null, null);
                } else {
                    final TvSeries tvSeries = getIntent().getParcelableExtra(DETAIL_DATA_TVSERIES);
                    getContentResolver().delete(Uri.parse(CONTENT_URI + "/ID/" + String.valueOf(tvSeries.getTvId())), null, null);
                }

                btnFav.setImageDrawable(notFav);
                isFavorited = false;

            } else {
                if (tipe == 0) {
                    final Movies movie = getIntent().getParcelableExtra(DETAIL_DATA_MOVIES);
                    ContentValues values = new ContentValues();
                    values.put(_ID , movie.getMovieId());
                    values.put(COL_2, movie.getMovieName());
                    values.put(COL_3, movie.getMovieDate());
                    values.put(COL_4, movie.getMovieOverview());
                    values.put(COL_5, movie.getMovieRate());
                    values.put(COL_6, movie.getMoviePopularity());
                    values.put(COL_7, movie.getMoviePoster());
                    values.put(COL_8, movie.getMovieBackDrop());
                    values.put(COL_9, 0);
                    getContentResolver().insert(CONTENT_URI, values);

                } else {
                    final TvSeries tvSeries = getIntent().getParcelableExtra(DETAIL_DATA_TVSERIES);
                    ContentValues values = new ContentValues();
                    values.put(_ID , tvSeries.getTvId());
                    values.put(COL_2, tvSeries.getTvName());
                    values.put(COL_3, tvSeries.getTvDate());
                    values.put(COL_4, tvSeries.getTvOverview());
                    values.put(COL_5, tvSeries.getTvRate());
                    values.put(COL_6, tvSeries.getTvPopularity());
                    values.put(COL_7, tvSeries.getTvPoster());
                    values.put(COL_8, tvSeries.getTvBackDrop());
                    values.put(COL_9, 1);
                    getContentResolver().insert(CONTENT_URI, values);
                }

                btnFav.setImageDrawable(isFav);
                isFavorited = true;
            }
        }
    }

    private void checkFavorite(int id){
        Cursor cursor = getContentResolver().query(Uri.parse(CONTENT_URI + "/ID/" + String.valueOf(id)), null, null, null, null);
        if(cursor.getCount() == 0) isFavorited = false;
        else isFavorited = true;
    }

    @Override
    public void preExecute() {

    }

    @Override
    public void postExecute(Cursor favorite) {

    }

    public static class DataObserverr extends ContentObserver {

        final Context context;

        public DataObserverr(Handler handler, Context context) {
            super(handler);
            this.context = context;
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);

        }
    }
}
