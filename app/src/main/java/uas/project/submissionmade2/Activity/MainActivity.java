package uas.project.submissionmade2.Activity;

import android.content.Intent;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.SearchView;

import uas.project.submissionmade2.Fragment.MovieFragment;
import uas.project.submissionmade2.R;
import uas.project.submissionmade2.Fragment.TvSeriesFragment;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,SearchView.OnQueryTextListener {
    private BottomNavigationView navigation;
    private ImageButton btnMenu;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;

            switch (item.getItemId()) {
                case R.id.navigation_movies:

                    fragment = new MovieFragment();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.frame_container, fragment, fragment.getClass().getSimpleName())
                            .commit();
                    return true;
                case R.id.navigation_tvseries:

                    fragment = new TvSeriesFragment();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.frame_container, fragment, fragment.getClass().getSimpleName())
                            .commit();
                    return true;
                case R.id.navigation_favorites:
                    Intent favorite = new Intent(getApplicationContext(), FavoriteActivity.class);
                    startActivity(favorite);
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        navigation = findViewById(R.id.navigation_view);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        btnMenu = findViewById(R.id.btnMenu);
        btnMenu.setOnClickListener(this);

        SearchView searchView;
        searchView = findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(this);

        Fragment fragment = new MovieFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame_container, fragment, fragment.getClass().getSimpleName())
                .commit();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("State",navigation.getSelectedItemId());
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.btnMenu){
            PopupMenu popup = new PopupMenu(MainActivity.this, btnMenu);
            //Inflating the Popup using xml file
            popup.getMenuInflater()
                    .inflate(R.menu.main_menu, popup.getMenu());

            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    if(item.getItemId() == R.id.action_change_settings){
                        Intent mIntent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
                        startActivity(mIntent);
                    }
                    else{
                        Intent mIntent = new Intent(getApplication(),SettingActivity.class);
                        startActivity(mIntent);
                    }
                    return true;
                }
            });

            popup.show();
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Fragment currFragment = getSupportFragmentManager().findFragmentById(R.id.frame_container);
        if(currFragment instanceof MovieFragment){
            Fragment fragment = new MovieFragment();
            Bundle bundle = new Bundle();
            bundle.putString("query",query);
            fragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frame_container, fragment, fragment.getClass().getSimpleName())
                    .commit();
        }
        else {
            Fragment fragment = new TvSeriesFragment();
            Bundle bundle = new Bundle();
            bundle.putString("query",query);
            fragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frame_container, fragment, fragment.getClass().getSimpleName())
                    .commit();
        }
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
}
