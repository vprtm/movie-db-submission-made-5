package uas.project.submissionmade2.Activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.CompoundButton;
import android.widget.Switch;

import uas.project.submissionmade2.Alarm.AlarmReceiver;
import uas.project.submissionmade2.R;

public class SettingActivity extends AppCompatActivity implements Switch.OnCheckedChangeListener {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Switch switchDaily,switchRelease;
    private Boolean dailySet, releaseSet;
    private AlarmReceiver alarmReceiver;
    private String timeDaily = "07:00";
    private String timeRelease = "08:00";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        switchDaily = findViewById(R.id.switchDaily);
        switchRelease = findViewById(R.id.switchToday);
        Context context = this;
        sharedPreferences = context.getSharedPreferences(getString(R.string.preferencefile), Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        checkSetting();

        switchDaily.setOnCheckedChangeListener(this);
        switchRelease.setOnCheckedChangeListener(this);

        alarmReceiver = new AlarmReceiver();

    }

    private void checkSetting() {

       if(sharedPreferences.getAll() != null){
           dailySet = sharedPreferences.getBoolean(getString(R.string.saveDaily),false);
           releaseSet = sharedPreferences.getBoolean(getString(R.string.saverelease),false);
       }
       else{
           dailySet = false;
           releaseSet = false;
           editor.putBoolean(getString(R.string.saveDaily),dailySet);
           editor.putBoolean(getString(R.string.saverelease),releaseSet);
           editor.apply();
       }

       if(dailySet){
           switchDaily.setChecked(true);
       }

       if(releaseSet){
           switchRelease.setChecked(true);
       }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()){
            case R.id.switchDaily:
                dailySet = isChecked;
                editor.putBoolean(getString(R.string.saveDaily),dailySet);
                editor.apply();

                if(isChecked){
                    alarmReceiver.setRepeatingAlarm(this, AlarmReceiver.TYPE_DAILY,
                            timeDaily);
                }
                else{
                    alarmReceiver.cancelAlarm(this, AlarmReceiver.TYPE_DAILY);
                }

                break;
            case R.id.switchToday:
                releaseSet = isChecked;
                editor.putBoolean(getString(R.string.saverelease),releaseSet);
                editor.apply();

                if(isChecked){
                    alarmReceiver.setRepeatingAlarm(this, AlarmReceiver.TYPE_RELEASE,
                            timeRelease);
                }
                else{
                    alarmReceiver.cancelAlarm(this, AlarmReceiver.TYPE_RELEASE);
                }
        }
    }
}
