package uas.project.submissionmade2.Helper;

import android.database.Cursor;

import java.util.ArrayList;

import uas.project.submissionmade2.Entity.Movies;
import uas.project.submissionmade2.Entity.TvSeries;

public class MappingHelper{

    public static ArrayList<Movies> moviesCursorToArrayList(Cursor res) {
        ArrayList<Movies> moviesArrayList = new ArrayList<>();

        while (res.moveToNext()) {
            Movies movies = new Movies();
            movies.setMovieId(res.getInt(0));
            movies.setMovieName(res.getString(1));
            movies.setMovieDate(res.getString(2));
            movies.setMovieOverview(res.getString(3));
            movies.setMovieRate(res.getFloat(4));
            movies.setMoviePopularity(res.getInt(5));
            movies.setMoviePoster(res.getString(6));
            movies.setMovieBackDrop(res.getString(7));
            moviesArrayList.add(movies);
        }

        return moviesArrayList;
    }

    public static ArrayList<TvSeries> tvCursorToArrayList(Cursor res) {
        ArrayList<TvSeries> tvSeriesArrayList = new ArrayList<>();

        while (res.moveToNext()) {
            TvSeries tvSeries = new TvSeries();
            tvSeries.setTvId(res.getInt(0));
            tvSeries.setTvName(res.getString(1));
            tvSeries.setTvDate(res.getString(2));
            tvSeries.setTvOverview(res.getString(3));
            tvSeries.setTvRate(res.getFloat(4));
            tvSeries.setTvPopularity(res.getInt(5));
            tvSeries.setTvPoster(res.getString(6));
            tvSeries.setTvBackDrop(res.getString(7));
            tvSeriesArrayList.add(tvSeries);
        }

        return tvSeriesArrayList;
    }
}
