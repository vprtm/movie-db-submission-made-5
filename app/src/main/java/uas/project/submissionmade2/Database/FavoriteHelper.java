package uas.project.submissionmade2.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import static android.provider.BaseColumns._ID;
import static uas.project.submissionmade2.Database.DatabaseContract.FavColumn.COL_7;
import static uas.project.submissionmade2.Database.DatabaseContract.FavColumn.COL_9;
import static uas.project.submissionmade2.Database.DatabaseContract.FavColumn.TABLE_NAME;

public class FavoriteHelper {

    private static final String DATABASE_TABLE = TABLE_NAME;
    private final DatabaseHelper dataBaseHelper;
    private static FavoriteHelper INSTANCE;

    private SQLiteDatabase database;

    private FavoriteHelper(Context context) {
        dataBaseHelper = new DatabaseHelper(context);
    }

    public static FavoriteHelper getInstance(Context context) {
        if (INSTANCE == null){
            synchronized (SQLiteOpenHelper.class){
                if (INSTANCE == null){
                    INSTANCE = new FavoriteHelper(context);
                }
            }
        }
        return INSTANCE;
    }

    public void open() throws SQLException {
        database = dataBaseHelper.getWritableDatabase();
    }

    public void close() {
        dataBaseHelper.close();

        if (database.isOpen())
            database.close();
    }

    public long insertDataProvider(ContentValues values) {
        return database.insert(DATABASE_TABLE, null, values);
    }

    public Cursor getAllDataPosterProvider() {
        return database.query(DATABASE_TABLE, new String[]{COL_7}, null, null, null, null, null);
    }

    public Cursor getAllDataMoviesProvider() {
        return database.query(DATABASE_TABLE, null, COL_9 + " = 0", null, null, null, null);
    }

    public Cursor getAllDataTvSeriesProvider() {
        return database.query(DATABASE_TABLE, null, COL_9 + " = 1", null, null, null, null);
    }

    public Cursor getDataByIdProvider(String id) {
        return database.query(DATABASE_TABLE, null, _ID + " = ?", new String[]{id} , null, null, null, null);
    }

    public Integer deleteDataProvider(String id) {
        return database.delete(DATABASE_TABLE, _ID + " = ?", new String[]{id});
    }




}
