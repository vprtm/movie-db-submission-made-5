package uas.project.submissionmade2.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "kontent.db";
    private static final int DATABASE_VERSION = 1;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    private static final String SQL_CREATE_TABLE_NOTE = String.format("CREATE TABLE %s"
                    + " (%s INTEGER PRIMARY KEY," +
                    " %s TEXT NOT NULL," +
                    " %s TEXT NOT NULL," +
                    " %s TEXT NOT NULL," +
                    " %s FLOAT NOT NULL," +
                    " %s INTEGER NOT NULL," +
                    " %s TEXT NOT NULL," +
                    " %s TEXT NOT NULL," +
                    " %s INTEGER NOT NULL)",
            DatabaseContract.FavColumn.TABLE_NAME,
            DatabaseContract.FavColumn._ID,
            DatabaseContract.FavColumn.COL_2,
            DatabaseContract.FavColumn.COL_3,
            DatabaseContract.FavColumn.COL_4,
            DatabaseContract.FavColumn.COL_5,
            DatabaseContract.FavColumn.COL_6,
            DatabaseContract.FavColumn.COL_7,
            DatabaseContract.FavColumn.COL_8,
            DatabaseContract.FavColumn.COL_9

    );


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_NOTE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+DatabaseContract.FavColumn.TABLE_NAME);
        onCreate(db);
    }

    public void insertData(int id,String name,String date,String overview,float rate,int popularity,String poster,String backdrop,int type) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseContract.FavColumn._ID,id);
        contentValues.put(DatabaseContract.FavColumn.COL_2,name);
        contentValues.put(DatabaseContract.FavColumn.COL_3,date);
        contentValues.put(DatabaseContract.FavColumn.COL_4,overview);
        contentValues.put(DatabaseContract.FavColumn.COL_5,rate);
        contentValues.put(DatabaseContract.FavColumn.COL_6,popularity);
        contentValues.put(DatabaseContract.FavColumn.COL_7,poster);
        contentValues.put(DatabaseContract.FavColumn.COL_8,backdrop);
        contentValues.put(DatabaseContract.FavColumn.COL_9,type);
        db.insert(DatabaseContract.FavColumn.TABLE_NAME,null ,contentValues);
    }

    public Cursor getAllDataPoster() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.rawQuery("select poster from "+ DatabaseContract.FavColumn.TABLE_NAME ,null);
    }

    public Cursor getAllDataMovies() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.rawQuery("select * from "+ DatabaseContract.FavColumn.TABLE_NAME + " where " + DatabaseContract.FavColumn.COL_9 + "=" + 0,null);
    }

    public Cursor getAllDataTvSeries() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.rawQuery("select * from "+ DatabaseContract.FavColumn.TABLE_NAME + " where " + DatabaseContract.FavColumn.COL_9 + "=" + 1,null);
    }

    public Cursor getDataById(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.rawQuery("select * from " + DatabaseContract.FavColumn.TABLE_NAME + " where " + DatabaseContract.FavColumn._ID + "=" + id,null);
    }


    public Integer deleteData (String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(DatabaseContract.FavColumn.TABLE_NAME, DatabaseContract.FavColumn._ID + " = ?",new String[] {id});
    }
}
