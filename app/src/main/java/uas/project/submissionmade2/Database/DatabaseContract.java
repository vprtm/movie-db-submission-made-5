package uas.project.submissionmade2.Database;

import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

public class DatabaseContract {
    public static final String AUTHORITY = "uas.project.submissionmade2";
    private static final String SCHEME = "content";

    private DatabaseContract(){}

    public static final class FavColumn implements BaseColumns {

        public static final String TABLE_NAME = "fav_table";
        public static final String COL_2 = "name";
        public static final String COL_3 = "date";
        public static final String COL_4 = "overview";
        public static final String COL_5 = "rate";
        public static final String COL_6 = "popularity";
        public static final String COL_7 = "poster";
        public static final String COL_8 = "backdrop";
        public static final String COL_9 = "type";

        // Base content yang digunakan untuk akses content provider
        public static final Uri CONTENT_URI = new Uri.Builder().scheme(SCHEME)
                .authority(AUTHORITY)
                .appendPath(TABLE_NAME)
                .build();

    }

    /*
    Digunakan untuk mempermudah akses data di dalam cursor dengan parameter nama column
    */
    public static String getColumnString(Cursor cursor, String columnName) {
        return cursor.getString(cursor.getColumnIndex(columnName));
    }

    public static int getColumnInt(Cursor cursor, String columnName) {
        return cursor.getInt(cursor.getColumnIndex(columnName));
    }

    public static double getColumnDouble(Cursor cursor, String columnName){
        return cursor.getDouble(cursor.getColumnIndex(columnName));
    }

}
