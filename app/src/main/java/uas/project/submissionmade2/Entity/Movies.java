package uas.project.submissionmade2.Entity;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import uas.project.submissionmade2.Database.DatabaseContract;

import static android.provider.BaseColumns._ID;
import static uas.project.submissionmade2.Database.DatabaseContract.getColumnDouble;
import static uas.project.submissionmade2.Database.DatabaseContract.getColumnInt;
import static uas.project.submissionmade2.Database.DatabaseContract.getColumnString;

public class Movies implements Parcelable {

    private int movieId,moviePopularity;
    private double movieRate;
    private String movieName,movieDate,movieOverview,moviePoster,movieBackDrop;

    public Movies(int movieId, int moviePopularity, double movieRate, String movieName, String movieDate, String movieOverview, String moviePoster, String movieBackDrop) {
        this.movieId = movieId;
        this.moviePopularity = moviePopularity;
        this.movieRate = movieRate;
        this.movieName = movieName;
        this.movieDate = movieDate;
        this.movieOverview = movieOverview;
        this.moviePoster = moviePoster;
        this.movieBackDrop = movieBackDrop;
    }

    public Movies(Cursor res){
        this.movieId = getColumnInt(res,_ID);
        this.moviePopularity = getColumnInt(res, DatabaseContract.FavColumn.COL_6);
        this.movieRate = getColumnDouble(res,DatabaseContract.FavColumn.COL_5);
        this.movieName = getColumnString(res,DatabaseContract.FavColumn.COL_2);
        this.movieDate = getColumnString(res,DatabaseContract.FavColumn.COL_3);
        this.movieOverview = getColumnString(res,DatabaseContract.FavColumn.COL_4);
        this.moviePoster = getColumnString(res,DatabaseContract.FavColumn.COL_7);
        this.movieBackDrop = getColumnString(res,DatabaseContract.FavColumn.COL_8);
    }

    public Movies(){

    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public int getMoviePopularity() {
        return moviePopularity;
    }

    public void setMoviePopularity(int moviePopularity) {
        this.moviePopularity = moviePopularity;
    }

    public double getMovieRate() {
        return movieRate;
    }

    public void setMovieRate(double movieRate) {
        this.movieRate = movieRate;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getMovieDate() {
        return movieDate;
    }

    public void setMovieDate(String movieDate) {
        this.movieDate = movieDate;
    }

    public String getMovieOverview() {
        return movieOverview;
    }

    public void setMovieOverview(String movieOverview) {
        this.movieOverview = movieOverview;
    }

    public String getMoviePoster() {
        return moviePoster;
    }

    public void setMoviePoster(String moviePoster) {
        this.moviePoster = moviePoster;
    }

    public String getMovieBackDrop() {
        return movieBackDrop;
    }

    public void setMovieBackDrop(String movieBackDrop) {
        this.movieBackDrop = movieBackDrop;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.movieId);
        dest.writeInt(this.moviePopularity);
        dest.writeDouble(this.movieRate);
        dest.writeString(this.movieName);
        dest.writeString(this.movieDate);
        dest.writeString(this.movieOverview);
        dest.writeString(this.moviePoster);
        dest.writeString(this.movieBackDrop);
    }

    protected Movies(Parcel in) {
        this.movieId = in.readInt();
        this.moviePopularity = in.readInt();
        this.movieRate = in.readDouble();
        this.movieName = in.readString();
        this.movieDate = in.readString();
        this.movieOverview = in.readString();
        this.moviePoster = in.readString();
        this.movieBackDrop = in.readString();
    }

    public static final Parcelable.Creator<Movies> CREATOR = new Parcelable.Creator<Movies>() {
        @Override
        public Movies createFromParcel(Parcel source) {
            return new Movies(source);
        }

        @Override
        public Movies[] newArray(int size) {
            return new Movies[size];
        }
    };
}
