package uas.project.submissionmade2.Entity;

import android.os.Parcel;
import android.os.Parcelable;

public class TvSeries implements Parcelable {

    private int tvId,tvPopularity;
    private double tvRate;
    private String tvName,tvDate,tvOverview,tvPoster,tvBackDrop;

    public TvSeries(int tvId, int tvPopularity, double tvRate, String tvName, String tvDate, String tvOverview, String tvPoster, String tvBackDrop) {
        this.tvId = tvId;
        this.tvPopularity = tvPopularity;
        this.tvRate = tvRate;
        this.tvName = tvName;
        this.tvDate = tvDate;
        this.tvOverview = tvOverview;
        this.tvPoster = tvPoster;
        this.tvBackDrop = tvBackDrop;
    }

    public TvSeries() {
    }

    public int getTvId() {
        return tvId;
    }

    public void setTvId(int tvId) {
        this.tvId = tvId;
    }

    public int getTvPopularity() {
        return tvPopularity;
    }

    public void setTvPopularity(int tvPopularity) {
        this.tvPopularity = tvPopularity;
    }

    public double getTvRate() {
        return tvRate;
    }

    public void setTvRate(double tvRate) {
        this.tvRate = tvRate;
    }

    public String getTvName() {
        return tvName;
    }

    public void setTvName(String tvName) {
        this.tvName = tvName;
    }

    public String getTvDate() {
        return tvDate;
    }

    public void setTvDate(String tvDate) {
        this.tvDate = tvDate;
    }

    public String getTvOverview() {
        return tvOverview;
    }

    public void setTvOverview(String tvOverview) {
        this.tvOverview = tvOverview;
    }

    public String getTvPoster() {
        return tvPoster;
    }

    public void setTvPoster(String tvPoster) {
        this.tvPoster = tvPoster;
    }

    public String getTvBackDrop() {
        return tvBackDrop;
    }

    public void setTvBackDrop(String tvBackDrop) {
        this.tvBackDrop = tvBackDrop;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.tvId);
        dest.writeInt(this.tvPopularity);
        dest.writeDouble(this.tvRate);
        dest.writeString(this.tvName);
        dest.writeString(this.tvDate);
        dest.writeString(this.tvOverview);
        dest.writeString(this.tvPoster);
        dest.writeString(this.tvBackDrop);
    }

    protected TvSeries(Parcel in) {
        this.tvId = in.readInt();
        this.tvPopularity = in.readInt();
        this.tvRate = in.readDouble();
        this.tvName = in.readString();
        this.tvDate = in.readString();
        this.tvOverview = in.readString();
        this.tvPoster = in.readString();
        this.tvBackDrop = in.readString();
    }

    public static final Creator<TvSeries> CREATOR = new Creator<TvSeries>() {
        @Override
        public TvSeries createFromParcel(Parcel source) {
            return new TvSeries(source);
        }

        @Override
        public TvSeries[] newArray(int size) {
            return new TvSeries[size];
        }
    };
}

