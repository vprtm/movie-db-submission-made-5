package uas.project.submissionmade2.Fragment;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import uas.project.submissionmade2.Activity.DetailActivity;
import uas.project.submissionmade2.Adapter.AdapterTvSeries;
import uas.project.submissionmade2.Adapter.ItemClickSupport;
import uas.project.submissionmade2.Entity.TvSeries;
import uas.project.submissionmade2.R;

import static uas.project.submissionmade2.Database.DatabaseContract.FavColumn.CONTENT_URI;
import static uas.project.submissionmade2.Helper.MappingHelper.tvCursorToArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteTvSeries extends Fragment implements LoadFavoriteCallback {

    private RecyclerView rvCategory;
    private AdapterTvSeries adapterTvSeries;
    private ArrayList<TvSeries> tvSeriesArrayList = new ArrayList<>();
    private ProgressBar progressBar;


    public FavoriteTvSeries() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorite_tv_series, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressBar = view.findViewById(R.id.progressTvFav);

        rvCategory = view.findViewById(R.id.recycler_view_tvSeries_fav);
        rvCategory.setHasFixedSize(true);

        HandlerThread handlerThread;
        FavoriteMovies.DataObserver myObserver;

        handlerThread = new HandlerThread("DataObserverr");
        handlerThread.start();

        Handler handler = new Handler(handlerThread.getLooper());
        myObserver = new FavoriteMovies.DataObserver(handler, getActivity().getApplicationContext());
        getActivity().getContentResolver().registerContentObserver(CONTENT_URI, true, myObserver);

        new LoadFavoriteAsync(getActivity().getApplicationContext(), this).execute();

        showRecyclerList();


        ItemClickSupport.addTo(rvCategory).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, final int position, View v) {
                showSelectedTvSeries(tvSeriesArrayList.get(position));
            }
        });
    }

    private void showRecyclerList() {
        rvCategory.setLayoutManager(new GridLayoutManager(getActivity().getApplicationContext(),2));
        adapterTvSeries = new AdapterTvSeries(getActivity().getApplicationContext());
        adapterTvSeries.setListTvSeries(tvSeriesArrayList);
        rvCategory.setAdapter(adapterTvSeries);
        progressBar.setVisibility(View.GONE);
    }

    private void showSelectedTvSeries(TvSeries tvSeries){
        Toast.makeText(getActivity(), "Kamu memilih "+tvSeries.getTvName(), Toast.LENGTH_SHORT).show();
        Intent detail = new Intent(getActivity(), DetailActivity.class);
        detail.putExtra(DetailActivity.DETAIL_DATA_TVSERIES, tvSeries);
        detail.putExtra("type",1);
        startActivity(detail);
    }

    @Override
    public void preExecute() {

    }

    @Override
    public void postExecute(Cursor favorite) {
        tvSeriesArrayList = tvCursorToArrayList(favorite);
        if (tvSeriesArrayList.size() > 0) {
            adapterTvSeries.setListTvChanged(tvSeriesArrayList);
        } else {
            adapterTvSeries.setListTvChanged(new ArrayList<TvSeries>());
        }
    }

    private static class LoadFavoriteAsync extends AsyncTask<Void, Void, Cursor> {

        private final WeakReference<Context> weakContext;
        private final WeakReference<LoadFavoriteCallback> weakCallback;

        private LoadFavoriteAsync(Context context, LoadFavoriteCallback callback) {
            weakContext = new WeakReference<>(context);
            weakCallback = new WeakReference<>(callback);
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            weakCallback.get().preExecute();
        }

        @Override
        protected Cursor doInBackground(Void... voids) {
            Context context = weakContext.get();
            return context.getContentResolver().query(Uri.parse(CONTENT_URI + "/TVSERIES"), null, null, null, null);
        }

        @Override
        protected void onPostExecute(Cursor res) {
            super.onPostExecute(res);
            weakCallback.get().postExecute(res);
        }
    }
}
