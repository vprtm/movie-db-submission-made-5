package uas.project.submissionmade2.Fragment;

import android.database.Cursor;

public interface LoadFavoriteCallback {
    void preExecute();

    void postExecute(Cursor favorite);
}
