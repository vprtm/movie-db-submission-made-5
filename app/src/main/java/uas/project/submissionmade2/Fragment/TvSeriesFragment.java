package uas.project.submissionmade2.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import uas.project.submissionmade2.Activity.DetailActivity;
import uas.project.submissionmade2.Adapter.AdapterTvSeries;
import uas.project.submissionmade2.Adapter.ItemClickSupport;
import uas.project.submissionmade2.R;
import uas.project.submissionmade2.Entity.TvSeries;


/**
 * A simple {@link Fragment} subclass.
 */
public class TvSeriesFragment extends Fragment {

    private RecyclerView rvCategory;
    private AdapterTvSeries adapterTvSeries;
    private String key = "f27cb1c0463d4fd3a8d54fbbe6cfa011";
    private String TAG = "RESULT_TAG";
    private ArrayList<TvSeries> tvSeriesArrayList = new ArrayList<>();
    private ProgressBar progressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressBar = view.findViewById(R.id.progressTv);

        if(getArguments() == null){
            addItem(null);
        }
        else{
            addItem(getArguments().getString("query"));
        }

        rvCategory = view.findViewById(R.id.recycler_view_tvSeries);
        rvCategory.setHasFixedSize(true);
        showRecyclerList();


        ItemClickSupport.addTo(rvCategory).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, final int position, View v) {
                showSelectedTvSeries(tvSeriesArrayList.get(position));
            }
        });

    }

    public TvSeriesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tv_series, container, false);


    }

    private void showRecyclerList() {
        rvCategory.setLayoutManager(new GridLayoutManager(getActivity().getApplicationContext(),2));
        adapterTvSeries = new AdapterTvSeries(getActivity());
        adapterTvSeries.setListTvSeries(tvSeriesArrayList);
        rvCategory.setAdapter(adapterTvSeries);
    }

    private void addItem(String query){

        String url;
        if(query == null){
            url = "https://api.themoviedb.org/3/tv/airing_today?api_key=" + key + "&language=" + getResources().getConfiguration().locale.toLanguageTag() + "&page=1";
        }
        else {
            url = "https://api.themoviedb.org/3/search/tv?api_key=" + key + "&language=" + getResources().getConfiguration().locale.toLanguageTag() + "&query=" + query + "&page=1";
        }
        tvSeriesArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String result = new String(responseBody);
                Log.d(TAG,"Success");
                try {
                    JSONObject responseObject = new JSONObject(result);
                    for(int i=0;i<responseObject.getJSONArray("results").length();i++){
                        TvSeries tvSeries = new TvSeries();
                        tvSeries.setTvId(responseObject.getJSONArray("results").getJSONObject(i).getInt("id"));
                        tvSeries.setTvName(responseObject.getJSONArray("results").getJSONObject(i).getString("original_name"));
                        tvSeries.setTvDate(responseObject.getJSONArray("results").getJSONObject(i).getString("first_air_date"));
                        tvSeries.setTvOverview(responseObject.getJSONArray("results").getJSONObject(i).getString("overview"));
                        tvSeries.setTvPoster(responseObject.getJSONArray("results").getJSONObject(i).getString("poster_path"));
                        tvSeries.setTvBackDrop(responseObject.getJSONArray("results").getJSONObject(i).getString("backdrop_path"));
                        tvSeries.setTvPopularity(responseObject.getJSONArray("results").getJSONObject(i).getInt("popularity"));
                        tvSeries.setTvRate(responseObject.getJSONArray("results").getJSONObject(i).getDouble("vote_average"));
                        tvSeriesArrayList.add(tvSeries);
                        Log.d(TAG,responseObject.getJSONArray("results").getJSONObject(i).getString("original_name"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressBar.setVisibility(View.GONE);
                adapterTvSeries.notifyDataSetChanged();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showSelectedTvSeries(TvSeries tvSeries){
        Toast.makeText(getActivity(), "Kamu memilih "+tvSeries.getTvName(), Toast.LENGTH_SHORT).show();
        Intent detail = new Intent(getActivity(), DetailActivity.class);
        detail.putExtra(DetailActivity.DETAIL_DATA_TVSERIES, tvSeries);
        detail.putExtra("type",1);
        startActivity(detail);
    }

}
