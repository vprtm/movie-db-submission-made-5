package uas.project.submissionmade2.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import uas.project.submissionmade2.Activity.DetailActivity;
import uas.project.submissionmade2.Adapter.AdapterMovies;
import uas.project.submissionmade2.Adapter.ItemClickSupport;
import uas.project.submissionmade2.Loader.MovieAsyncTaskLoader;
import uas.project.submissionmade2.Entity.Movies;
import uas.project.submissionmade2.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class MovieFragment extends Fragment implements LoaderManager.LoaderCallbacks<ArrayList<Movies>> {

    private RecyclerView rvCategory;
    private ArrayList<Movies> moviesArrayList = new ArrayList<>();
    private AdapterMovies adapterMovies;
    private ProgressBar progressBar;
    private String key = "f27cb1c0463d4fd3a8d54fbbe6cfa011";
    private String TAG = "RESULT_TAG";

    public MovieFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_movie, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBar = view.findViewById(R.id.progressMovie);

        if(getArguments() == null){
            Bundle loadBundle = new Bundle();
            getLoaderManager().initLoader(0, loadBundle, this);
        }
        else{
            searchItem(getArguments().getString("query"));
        }

        rvCategory = view.findViewById(R.id.recycler_view_movie);
        rvCategory.setHasFixedSize(true);
        showRecyclerList();

        ItemClickSupport.addTo(rvCategory).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, final int position, View v) {
                showSelectedMovies(moviesArrayList.get(position));
            }
        });

    }

    private void showRecyclerList() {
        rvCategory.setLayoutManager(new GridLayoutManager(getActivity().getApplicationContext(),2));
        adapterMovies = new AdapterMovies(getActivity());
        adapterMovies.setListMovies(moviesArrayList);
        rvCategory.setAdapter(adapterMovies);
    }


    private void showSelectedMovies(Movies movies){
        Toast.makeText(getActivity(), "Kamu memilih "+movies.getMovieName(), Toast.LENGTH_SHORT).show();
        Intent detail = new Intent(getActivity(), DetailActivity.class);
        detail.putExtra(DetailActivity.DETAIL_DATA_MOVIES, movies);
        detail.putExtra("type",0);
        startActivity(detail);
    }

    @NonNull
    @Override
    public Loader<ArrayList<Movies>> onCreateLoader(int i, @Nullable Bundle bundle) {
        return new MovieAsyncTaskLoader(getActivity());

    }

    @Override
    public void onLoadFinished(@NonNull Loader<ArrayList<Movies>> loader, ArrayList<Movies> movies) {
        moviesArrayList = movies;
        adapterMovies.setListMovies(movies);
        progressBar.setVisibility(View.GONE);
        adapterMovies.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(@NonNull Loader<ArrayList<Movies>> loader) {
        adapterMovies.setListMovies(null);
    }

    private void searchItem(String query){
        moviesArrayList = new ArrayList<>();
        String url = "https://api.themoviedb.org/3/search/movie?api_key="+ key + "&language=" + getResources().getConfiguration().locale.toLanguageTag() + "&query=" + query + "&page=1";
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String result = new String(responseBody);
                Log.d(TAG,"Success");

                try {
                    JSONObject responseObject = new JSONObject(result);
                    for(int i=0;i<responseObject.getJSONArray("results").length();i++){

                        Movies movies = new Movies();
                        movies.setMovieId(responseObject.getJSONArray("results").getJSONObject(i).getInt("id"));
                        movies.setMovieName(responseObject.getJSONArray("results").getJSONObject(i).getString("original_title"));
                        movies.setMovieDate(responseObject.getJSONArray("results").getJSONObject(i).getString("release_date"));
                        movies.setMovieOverview(responseObject.getJSONArray("results").getJSONObject(i).getString("overview"));
                        movies.setMoviePoster(responseObject.getJSONArray("results").getJSONObject(i).getString("poster_path"));
                        movies.setMovieBackDrop(responseObject.getJSONArray("results").getJSONObject(i).getString("backdrop_path"));
                        movies.setMoviePopularity(responseObject.getJSONArray("results").getJSONObject(i).getInt("popularity"));
                        movies.setMovieRate(responseObject.getJSONArray("results").getJSONObject(i).getDouble("vote_average"));
                        moviesArrayList.add(movies);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressBar.setVisibility(View.GONE);
                adapterMovies.notifyDataSetChanged();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
