package uas.project.submissionmade2.Fragment;


import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import uas.project.submissionmade2.Activity.DetailActivity;
import uas.project.submissionmade2.Adapter.AdapterMovies;
import uas.project.submissionmade2.Adapter.ItemClickSupport;
import uas.project.submissionmade2.Entity.Movies;
import uas.project.submissionmade2.R;

import static uas.project.submissionmade2.Database.DatabaseContract.FavColumn.CONTENT_URI;
import static uas.project.submissionmade2.Helper.MappingHelper.moviesCursorToArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteMovies extends Fragment implements LoadFavoriteCallback {

    private RecyclerView rvCategory;
    private ArrayList<Movies> moviesArrayList = new ArrayList<>();
    private AdapterMovies adapterMovies;
    private ProgressBar progressBar;

    public FavoriteMovies() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorite_movies, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBar = view.findViewById(R.id.progressMovieFav);
        rvCategory = view.findViewById(R.id.recycler_view_movie_fav);
        rvCategory.setHasFixedSize(true);

        HandlerThread handlerThread;
        DataObserver myObserver;

        handlerThread = new HandlerThread("DataObserverr");
        handlerThread.start();

        Handler handler = new Handler(handlerThread.getLooper());
        myObserver = new DataObserver(handler, getActivity().getApplicationContext());
        getActivity().getContentResolver().registerContentObserver(CONTENT_URI, true, myObserver);

        new LoadFavoriteAsync(getActivity().getApplicationContext(), this).execute();

        showRecyclerList();

        ItemClickSupport.addTo(rvCategory).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, final int position, View v) {
                showSelectedMovies(moviesArrayList.get(position));
            }
        });
    }

    private void showRecyclerList() {
        rvCategory.setLayoutManager(new GridLayoutManager(getActivity().getApplicationContext(),2));
        adapterMovies = new AdapterMovies(getActivity().getApplicationContext());
        adapterMovies.setListMovies(moviesArrayList);
        rvCategory.setAdapter(adapterMovies);
        progressBar.setVisibility(View.GONE);
    }

    private void  showSelectedMovies(Movies movies){
        Toast.makeText(getActivity(), "Kamu memilih "+movies.getMovieName(), Toast.LENGTH_SHORT).show();
        Intent detail = new Intent(getActivity(), DetailActivity.class);
        detail.putExtra(DetailActivity.DETAIL_DATA_MOVIES, movies);
        detail.putExtra("type",0);
        startActivity(detail);
    }

    @Override
    public void preExecute() {

    }

    @Override
    public void postExecute(Cursor movie) {
        moviesArrayList = moviesCursorToArrayList(movie);
        if (moviesArrayList.size() > 0) {
            adapterMovies.setListMoviesChanged(moviesArrayList);
        } else {
            adapterMovies.setListMoviesChanged(new ArrayList<Movies>());
        }
    }

    private static class LoadFavoriteAsync extends AsyncTask<Void, Void, Cursor> {

        private final WeakReference<Context> weakContext;
        private final WeakReference<LoadFavoriteCallback> weakCallback;

        private LoadFavoriteAsync(Context context, LoadFavoriteCallback callback) {
            weakContext = new WeakReference<>(context);
            weakCallback = new WeakReference<>(callback);
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            weakCallback.get().preExecute();
        }

        @Override
        protected Cursor doInBackground(Void... voids) {
            Context context = weakContext.get();
            return context.getContentResolver().query(Uri.parse(CONTENT_URI + "/MOVIES"), null, null, null, null);
        }

        @Override
        protected void onPostExecute(Cursor res) {
            super.onPostExecute(res);
            weakCallback.get().postExecute(res);
        }
    }

    public static class DataObserver extends ContentObserver {

        final Context context;

        public DataObserver(Handler handler, Context context) {
            super(handler);
            this.context = context;
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
        }
    }
}
