package uas.project.submissionmade2.Loader;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import uas.project.submissionmade2.Entity.Movies;

public class MovieAsyncTaskLoader extends AsyncTaskLoader<ArrayList<Movies>> {

    private ArrayList<Movies> movieData;
    private boolean mHasResult = false;
    private String key = "f27cb1c0463d4fd3a8d54fbbe6cfa011";
    private String TAG = "RESULT_TAG";

    public MovieAsyncTaskLoader(@NonNull Context context) {
        super(context);
        onContentChanged();
    }

    @Nullable
    @Override
    public ArrayList<Movies> loadInBackground() {
        AsyncHttpClient client = new SyncHttpClient();

        final ArrayList<Movies> moviesArrayList = new ArrayList<>();
        String url = "https://api.themoviedb.org/3/movie/upcoming?api_key=" + key + "&language=" + getContext().getResources().getConfiguration().locale.toLanguageTag() + "&page=1";
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String result = new String(responseBody);
                Log.d(TAG, "Success");
                try {
                    JSONObject responseObject = new JSONObject(result);
                    for (int i = 0; i < responseObject.getJSONArray("results").length(); i++) {
                        Movies movies = new Movies();
                        movies.setMovieId(responseObject.getJSONArray("results").getJSONObject(i).getInt("id"));
                        movies.setMovieName(responseObject.getJSONArray("results").getJSONObject(i).getString("original_title"));
                        movies.setMovieDate(responseObject.getJSONArray("results").getJSONObject(i).getString("release_date"));
                        movies.setMovieOverview(responseObject.getJSONArray("results").getJSONObject(i).getString("overview"));
                        movies.setMoviePoster(responseObject.getJSONArray("results").getJSONObject(i).getString("poster_path"));
                        movies.setMovieBackDrop(responseObject.getJSONArray("results").getJSONObject(i).getString("backdrop_path"));
                        movies.setMoviePopularity(responseObject.getJSONArray("results").getJSONObject(i).getInt("popularity"));
                        movies.setMovieRate(responseObject.getJSONArray("results").getJSONObject(i).getDouble("vote_average"));
                        moviesArrayList.add(movies);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d(TAG, error.getMessage());
            }
        });

        return moviesArrayList;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        if (takeContentChanged())
            forceLoad();
        else if (mHasResult)
            deliverResult(movieData);
    }

    @Override
    public void deliverResult(@Nullable ArrayList<Movies> data) {
        movieData = data;
        mHasResult = true;
        super.deliverResult(data);
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();
        if (mHasResult) {
            movieData = null;
            mHasResult = false;
        }
    }
}
