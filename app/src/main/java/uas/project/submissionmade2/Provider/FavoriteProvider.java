package uas.project.submissionmade2.Provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;

import uas.project.submissionmade2.Database.FavoriteHelper;
import uas.project.submissionmade2.Fragment.FavoriteMovies;

import static uas.project.submissionmade2.Database.DatabaseContract.AUTHORITY;
import static uas.project.submissionmade2.Database.DatabaseContract.FavColumn.CONTENT_URI;
import static uas.project.submissionmade2.Database.DatabaseContract.FavColumn.TABLE_NAME;

public class FavoriteProvider extends ContentProvider {
    /*
  Integer digunakan sebagai identifier antara select all sama select by id
   */
    private static final int FAV = 1;
    private static final int MOVIE = 2;
    private static final int TV = 3;
    private static final int POSTER = 4;
    private static final int ID = 5;
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    /*
    Uri matcher untuk mempermudah identifier dengan menggunakan integer
    misal
    uri com.dicoding.picodiploma.mynotesapp dicocokan dengan integer 1
    uri com.dicoding.picodiploma.mynotesapp/# dicocokan dengan integer 2
     */
    static {

        // content://com.dicoding.picodiploma.mynotesapp/movie
        sUriMatcher.addURI(AUTHORITY, TABLE_NAME, FAV);
        sUriMatcher.addURI(AUTHORITY, TABLE_NAME + "/MOVIES" , MOVIE);
        sUriMatcher.addURI(AUTHORITY, TABLE_NAME + "/TVSERIES" , TV);
        sUriMatcher.addURI(AUTHORITY, TABLE_NAME + "/POSTER", TV);
        sUriMatcher.addURI(AUTHORITY, TABLE_NAME + "/ID" + "/#", ID);
    }

    private FavoriteHelper favoriteHelper;

    @Override
    public boolean onCreate() {
        favoriteHelper = FavoriteHelper.getInstance(getContext());
        return true;
    }

    /*
    Method query digunakan ketika ingin menjalankan query Select
    Return cursor
     */
    @Override
    public Cursor query(@NonNull Uri uri, String[] strings, String s, String[] strings1, String s1) {
        favoriteHelper.open();
        Cursor cursor;
        switch (sUriMatcher.match(uri)) {
            case MOVIE:
                cursor = favoriteHelper.getAllDataMoviesProvider();
                break;
            case TV:
                cursor = favoriteHelper.getAllDataTvSeriesProvider();
                break;
            case POSTER:
                cursor = favoriteHelper.getAllDataPosterProvider();
                break;
            case ID:
                cursor = favoriteHelper.getDataByIdProvider(uri.getLastPathSegment());
                break;
            default:
                cursor = null;
                break;
        }

        return cursor;
    }


    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }


    @Override
    public Uri insert(@NonNull Uri uri, ContentValues contentValues) {
        favoriteHelper.open();
        long added;
        switch (sUriMatcher.match(uri)) {
            case FAV:
                added = favoriteHelper.insertDataProvider(contentValues);
                getContext().getContentResolver().notifyChange(CONTENT_URI, new FavoriteMovies.DataObserver(new Handler(), getContext()));
                break;
            default:
                added = 0;
                break;
        }

        return Uri.parse(CONTENT_URI + "/" + added);
    }


    @Override
    public int update(@NonNull Uri uri, ContentValues contentValues, String s, String[] strings) {
        return 0;
    }

    @Override
    public int delete(@NonNull Uri uri, String s, String[] strings) {
        favoriteHelper.open();
        int deleted;
        switch (sUriMatcher.match(uri)) {
            case ID:
                deleted = favoriteHelper.deleteDataProvider(uri.getLastPathSegment());
                getContext().getContentResolver().notifyChange(CONTENT_URI, new FavoriteMovies.DataObserver(new Handler(), getContext()));
                break;
            default:
                deleted = 0;
                break;
        }

        return deleted;
    }

}
