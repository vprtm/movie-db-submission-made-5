package uas.project.submissionmade2.Alarm;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import uas.project.submissionmade2.Activity.DetailActivity;
import uas.project.submissionmade2.Activity.MainActivity;
import uas.project.submissionmade2.Entity.Movies;
import uas.project.submissionmade2.R;

public class AlarmReceiver extends BroadcastReceiver {

    public static final String TYPE_DAILY = "DailyAlarm";
    public static final String TYPE_RELEASE = "ReleaseAlarm";
    public static final String EXTRA_TYPE = "type";

    private final static int ID_DAILY = 100;
    private final static int ID_RELEASE = 101;

    private String key = "f27cb1c0463d4fd3a8d54fbbe6cfa011";
    private String TAG = "TAG";

    @Override
    public void onReceive(final Context context, Intent intent) {
        String type = intent.getStringExtra(EXTRA_TYPE);
        if(type.equals(TYPE_DAILY)){
            showAlarmNotification(context,TYPE_DAILY, 110, null);
        }
        else if(type.equals(TYPE_RELEASE)){
            getTodayRelease(context, new MovieCallback() {
                @Override
                public void onCallback(ArrayList<Movies> movies) {
                    for(int i=0; i<2; i++){
                        showAlarmNotification(context,TYPE_RELEASE,111+i,movies.get(i));
                    }
                }
            });
        }
    }

    private void getTodayRelease(final Context context, final MovieCallback callback) {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c);

        String url = "https://api.themoviedb.org/3/discover/movie?api_key=" + key + "&primary_release_date.gte=" + formattedDate + "&primary_release_date.lte=" + formattedDate + "&language=" + context.getResources().getConfiguration().locale.toString() + "&page=1";
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new AsyncHttpResponseHandler() {
            ArrayList<Movies> moviesArrayList = new ArrayList<>();
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String result = new String(responseBody);
                Log.d(TAG,"Success");

                try {
                    JSONObject responseObject = new JSONObject(result);
                    for(int i=0;i<responseObject.getJSONArray("results").length();i++){

                        Movies movies = new Movies();
                        movies.setMovieId(responseObject.getJSONArray("results").getJSONObject(i).getInt("id"));
                        movies.setMovieName(responseObject.getJSONArray("results").getJSONObject(i).getString("original_title"));
                        movies.setMovieDate(responseObject.getJSONArray("results").getJSONObject(i).getString("release_date"));
                        movies.setMovieOverview(responseObject.getJSONArray("results").getJSONObject(i).getString("overview"));
                        movies.setMoviePoster(responseObject.getJSONArray("results").getJSONObject(i).getString("poster_path"));
                        movies.setMovieBackDrop(responseObject.getJSONArray("results").getJSONObject(i).getString("backdrop_path"));
                        movies.setMoviePopularity(responseObject.getJSONArray("results").getJSONObject(i).getInt("popularity"));
                        movies.setMovieRate(responseObject.getJSONArray("results").getJSONObject(i).getDouble("vote_average"));
                        moviesArrayList.add(movies);
                    }
                    callback.onCallback(moviesArrayList);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
            }

        });
    }

    public AlarmReceiver() {
    }

    public void setRepeatingAlarm(Context context, String type, String time) {

        // Validasi inputan waktu terlebih dahulu
        if (isDateInvalid(time, TIME_FORMAT)) return;

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra(EXTRA_TYPE, type);

        String timeArray[] = time.split(":");

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeArray[0]));
        calendar.set(Calendar.MINUTE, Integer.parseInt(timeArray[1]));
        calendar.set(Calendar.SECOND, 0);

        PendingIntent pendingIntent;

        if(type.equals(TYPE_DAILY)) pendingIntent = PendingIntent.getBroadcast(context, ID_DAILY, intent, 0);
        else pendingIntent = PendingIntent.getBroadcast(context, ID_RELEASE, intent,0);

        if (alarmManager != null) {
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
            Toast.makeText(context, "Reminder set up", Toast.LENGTH_SHORT).show();
        }
    }


    public void cancelAlarm(Context context, String type) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        PendingIntent pendingIntent;

        if(type.equals(TYPE_DAILY)) pendingIntent = PendingIntent.getBroadcast(context, ID_DAILY, intent, 0);
        else pendingIntent = PendingIntent.getBroadcast(context, ID_RELEASE, intent, 0);

        pendingIntent.cancel();

        if (alarmManager != null) {
            alarmManager.cancel(pendingIntent);
            Toast.makeText(context, "Reminder set off", Toast.LENGTH_SHORT).show();
        }

    }

    private void showAlarmNotification(final Context context,String type, int notifId, Movies movies) {
        String CHANNEL_ID = "Channel_1";
        String CHANNEL_NAME = "AlarmManager channel";
        NotificationCompat.Builder builder;
        NotificationManager notificationManagerCompat;

        if(type.equals(TYPE_DAILY))
        {
            Intent startIntent = new Intent(context, MainActivity.class);
            PendingIntent pendingIntent = TaskStackBuilder.create(context)
                    .addNextIntent(startIntent)
                    .getPendingIntent(notifId, PendingIntent.FLAG_UPDATE_CURRENT);

            notificationManagerCompat = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                    .setSmallIcon(R.drawable.baseline_favorite_black_18)
                    .setContentTitle(context.getString(R.string.dailynotif))
                    .setContentText(context.getString(R.string.dailyremindertext))
                    .setColor(ContextCompat.getColor(context, android.R.color.transparent))
                    .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                    .setContentIntent(pendingIntent)
                    .setSound(alarmSound);
        }
        else{

            Intent startIntent = new Intent(context, DetailActivity.class);
            startIntent.putExtra(DetailActivity.DETAIL_DATA_MOVIES, movies);
            startIntent.putExtra("type",0);
            PendingIntent pendingIntent = TaskStackBuilder.create(context)
                    .addParentStack(MainActivity.class)
                    .addNextIntent(startIntent)
                    .getPendingIntent(notifId, PendingIntent.FLAG_UPDATE_CURRENT);

            notificationManagerCompat = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                    .setSmallIcon(R.drawable.baseline_favorite_black_18)
                    .setContentTitle(context.getString(R.string.releasenotif))
                    .setContentText(movies.getMovieName() + context.getString(R.string.releaseremindertext))
                    .setColor(ContextCompat.getColor(context, android.R.color.transparent))
                    .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                    .setContentIntent(pendingIntent)
                    .setSound(alarmSound);
        }

        /*
        Untuk android Oreo ke atas perlu menambahkan notification channel
        Materi ini akan dibahas lebih lanjut di modul extended
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            /* Create or update. */
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_DEFAULT);

            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{1000, 1000, 1000, 1000, 1000});

            builder.setChannelId(CHANNEL_ID);

            if (notificationManagerCompat != null) {
                notificationManagerCompat.createNotificationChannel(channel);
            }
        }

        Notification notification = builder.build();

        if (notificationManagerCompat != null) {
            notificationManagerCompat.notify(notifId, notification);
        }

    }

    private final static String TIME_FORMAT = "HH:mm";

    // Metode ini digunakan untuk validasi date dan time
    public boolean isDateInvalid(String date, String format) {
        try {
            DateFormat df = new SimpleDateFormat(format, Locale.getDefault());
            df.setLenient(false);
            df.parse(date);
            return false;
        } catch (ParseException e) {
            return true;
        }
    }

    private void showToast(Context context, String title, String message) {
        Toast.makeText(context, title + " : " + message, Toast.LENGTH_LONG).show();
    }

    public interface MovieCallback{
        void onCallback(ArrayList<Movies> movies);
    }
}
