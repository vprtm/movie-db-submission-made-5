package uas.project.submissionmade2.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import uas.project.submissionmade2.Fragment.FavoriteMovies;
import uas.project.submissionmade2.Fragment.FavoriteTvSeries;

public class PagerAdapter extends FragmentStatePagerAdapter {

    int tabCount;

    public PagerAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                FavoriteMovies favoriteMovies = new FavoriteMovies();
                return favoriteMovies;
            case 1:
                FavoriteTvSeries favoriteTvSeries = new FavoriteTvSeries();
                return favoriteTvSeries;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
