package uas.project.submissionmade2.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import uas.project.submissionmade2.Entity.Movies;
import uas.project.submissionmade2.R;

public class AdapterMovies extends RecyclerView.Adapter<AdapterMovies.CategoryViewHolder> {

    private Context context;

    public AdapterMovies(Context context) {
        this.context = context;
    }

    public ArrayList<Movies> getListMovies() {
        return listMovies;
    }

    public void setListMovies(ArrayList<Movies> listMovies) {
        this.listMovies = listMovies;
    }

    private ArrayList<Movies> listMovies;

    public void setListMoviesChanged(ArrayList<Movies> listNotes) {
        this.listMovies.clear();
        this.listMovies.addAll(listNotes);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemRow = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycle_item, viewGroup, false);
        return new CategoryViewHolder(itemRow);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder categoryViewHolder, int i) {
        String url = "https://image.tmdb.org/t/p/w600_and_h900_bestv2";
        categoryViewHolder.tvRate.setText(String.valueOf(getListMovies().get(i).getMovieRate())+" ");
        categoryViewHolder.tvDate.setText(String.valueOf(getListMovies().get(i).getMovieDate()));
        categoryViewHolder.tvName.setText(getListMovies().get(i).getMovieName());
        Glide.with(categoryViewHolder.itemView.getContext())
                .load(url + getListMovies().get(i).getMoviePoster())
                .into(categoryViewHolder.imgPhoto);
    }

    @Override
    public int getItemCount() {
        return getListMovies().size();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        TextView tvRate,tvDate,tvName;
        ImageView imgPhoto;
        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            tvRate = itemView.findViewById(R.id.tvRate);
            imgPhoto = itemView.findViewById(R.id.img);
            tvDate = itemView.findViewById(R.id.textDate);
            tvName = itemView.findViewById(R.id.textName);
        }
    }
}
