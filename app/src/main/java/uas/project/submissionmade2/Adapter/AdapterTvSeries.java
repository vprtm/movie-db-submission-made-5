package uas.project.submissionmade2.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import uas.project.submissionmade2.Entity.TvSeries;
import uas.project.submissionmade2.R;

public class AdapterTvSeries extends RecyclerView.Adapter<AdapterTvSeries.CategoryViewHolder> {

    private Context context;
    private ArrayList<TvSeries> listTvSeries;

    public AdapterTvSeries(Context context) {
        this.context = context;
    }

    public ArrayList<TvSeries> getListTvSeries() {
        return listTvSeries;
    }

    public void setListTvSeries(ArrayList<TvSeries> listTvSeries) {
        this.listTvSeries = listTvSeries;
    }

    public void setListTvChanged(ArrayList<TvSeries> listNotes) {
        this.listTvSeries.clear();
        this.listTvSeries.addAll(listNotes);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemRow = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycle_item, viewGroup, false);
        return new AdapterTvSeries.CategoryViewHolder(itemRow);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder categoryViewHolder, int i) {
        String url = "https://image.tmdb.org/t/p/w600_and_h900_bestv2";
        categoryViewHolder.tvRating.setText(String.valueOf(getListTvSeries().get(i).getTvRate())+" ");
        categoryViewHolder.tvDate.setText(String.valueOf(getListTvSeries().get(i).getTvDate()));
        categoryViewHolder.tvName.setText(getListTvSeries().get(i).getTvName());
        Glide.with(categoryViewHolder.itemView.getContext())
                .load(url + getListTvSeries().get(i).getTvPoster())
                .into(categoryViewHolder.imgPhoto);
    }

    @Override
    public int getItemCount() {
        return getListTvSeries().size();
    }


    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        TextView tvRating,tvName,tvDate;
        ImageView imgPhoto;
        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            imgPhoto = itemView.findViewById(R.id.img);
            tvRating = itemView.findViewById(R.id.tvRate);
            tvDate = itemView.findViewById(R.id.textDate);
            tvName = itemView.findViewById(R.id.textName);
        }
    }
}
